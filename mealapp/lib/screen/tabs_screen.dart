import 'package:flutter/material.dart';
import 'package:mealapp/screen/categories_screen.dart';
import 'package:mealapp/screen/favorites_screen.dart';
import 'package:mealapp/wigdet/main_drawer.dart';

import '../model/meal.dart';

class TabsScreen extends StatefulWidget {
  final List<Meal> favoritedMeals;

  const TabsScreen(this.favoritedMeals);

  @override
  State<TabsScreen> createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {

  late List<Map<String,Object>> _pages;
  int _selectedPageindex = 0;

  @override
  void initState() {
    super.initState();
    _pages=[
      {'page':CategorieScreen(), 'title':'Categories'},
      {'page':FavoriteScreen(widget.favoritedMeals),'title':'Your Favorite'}
    ];
  }

  void _selectPage(int index){
    setState((){
      _selectedPageindex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectedPageindex]['title'] as String),
      ),
      drawer: MainDrawer(),
      body: _pages[_selectedPageindex]['page'] as Widget,
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        backgroundColor: Theme.of(context).primaryColor,
        unselectedItemColor: Colors.white,
        selectedItemColor: Theme.of(context).accentColor,
        currentIndex: _selectedPageindex,
        //type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.category),
            label: 'Categories'
            ),
          BottomNavigationBarItem(
              backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.star),
            label: 'Favorites',
          ),
        ],
      ),
    );
  }
}
