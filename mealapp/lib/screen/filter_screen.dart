import 'package:flutter/material.dart';
import 'package:mealapp/wigdet/main_drawer.dart';

class FiltersScreen extends StatefulWidget {
  static const routeName = '/filters';

  final Function saveFilters;
  final Map<String, bool> currentFilters;

  const FiltersScreen(this.currentFilters,this.saveFilters);

  @override
  State<FiltersScreen> createState() => _FiltersScreenState();
}

class _FiltersScreenState extends State<FiltersScreen> {
  bool _glutenFree = false;
  bool _vegetarian = false;
  bool _vegan = false;
  bool _lactoseFree = false;


  @override
  initState() {
    super.initState();
    _glutenFree = widget.currentFilters['gluten'] as bool;
    _lactoseFree = widget.currentFilters['lactose'] as bool;
    _vegetarian = widget.currentFilters['vagetarian'] as bool;
    _vegan = widget.currentFilters['vegan'] as bool;
  }

  Widget buildSwitchTile(
      bool currentValue, String title, String subTitle, Function(bool) update) {
    return SwitchListTile(
      value: currentValue,
      title: Text(title),
      subtitle: Text(subTitle),
      onChanged: update,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Filters'),
          actions: [
            IconButton(
                onPressed: () {
                  final Map<String, bool> _selectedFilters = {
                    'gluten': _glutenFree,
                    'lactose': _lactoseFree,
                    'vegan': _vegan,
                    'vagetarian': _vegetarian,
                  };
                  widget.saveFilters(_selectedFilters);
                },
                icon: Icon(Icons.save))
          ],
        ),
        drawer: MainDrawer(),
        body: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(20),
              child: Text(
                'Adjust yout meal selection',
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
            Expanded(
              child: ListView(
                children: <Widget>[
                  buildSwitchTile(_glutenFree, 'Gluten-free',
                      'Only Include gluten-free meals', (value) {
                    setState(() {
                      _glutenFree = value;
                    });
                  }),
                  buildSwitchTile(_vegetarian, 'Vagetarian',
                      'Only Include Vagetarian meals', (value) {
                    setState(() {
                      _vegetarian = value;
                    });
                  }),
                  buildSwitchTile(_lactoseFree, 'Lactose-Free',
                      'Only Include Lactose-Free meals', (value) {
                    setState(() {
                      _lactoseFree = value;
                    });
                  }),
                  buildSwitchTile(_vegan, 'Vagen', 'Only Include Vagen meals',
                      (value) {
                    setState(() {
                      _vegan = value;
                    });
                  }),
                ],
              ),
            )
          ],
        ));
  }
}
